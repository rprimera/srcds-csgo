FROM kmallea/csgo:latest

ENV STEAM_DIR /home/steam

COPY --chown=steam:steam containerfs ${STEAM_DIR}/
